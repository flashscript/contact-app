import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import Dexie from 'dexie'

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.sass']
})
export class ContactsComponent implements OnInit {
  @Output() id = new EventEmitter<number>()
  db: any
  contacts: any[] = []

  constructor()
  {
    this.db = new Dexie('database')
    this.db.version(1).stores({
      contacts: '++id, name, phone, address, birthdate'
    })
    this.db.open().catch(err => console.log(err))
    this.db.table('contacts').toArray().then((res: any) => this.contacts = res)
  }

  ngOnInit(): void {
  }

  update(val: number)
  {
    this.id.emit(val)
  }

}
