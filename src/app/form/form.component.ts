import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import Dexie from 'dexie'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {
	data: FormGroup
  db: any
  @Input() index: number | null = null
	@Output() close = new EventEmitter<boolean>();
  name = new FormControl('')

  constructor(private _builder: FormBuilder) 
  {
  	this.data = this._builder.group({
      id: [null],
  		name: ['', Validators.required],
  		phone: [null, Validators.required],
  		address: [null, Validators.required],
  		birthdate: [null, Validators.required]
  	})
    this.db = new Dexie('database')
    this.db.version(1).stores({
      contacts: '++id, name, phone, address, birthdate'
    })
    this.db.open().catch(err => console.log(err))

  }

  ngOnInit(): void 
  {
    if (this.index)
      this.db.table('contacts').get(this.index, (item: any) => {
        this.data.setValue({
          id: this.index,
          name: item.name,
          phone: item.phone,
          address: item.address,
          birthdate: item.birthdate
        })
      })
  }

  upsert(val: any)
  {
    if (!this.index)
      delete val.id

    this.db.table('contacts').put(val, 'id')
    this.data.setValue({
      id: null,
      name: '',
      phone: '',
      address: '',
      birthdate: ''
    })
    this.close.emit(false)
  }

  async destroy()
  {
    if (this.index)
      await this.db.table('contacts').delete(this.index)
    this.close.emit(false)
  }
}
