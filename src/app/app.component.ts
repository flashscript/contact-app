import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
	form: boolean = false
	key: number | null = null

	onClosed(value: boolean)
	{
		this.form = value
	}

	onUpdated(val: number)
	{
		this.key = val
		this.form = true
	}

	create()
	{
		this.key = null
		this.form = true
	}

}
