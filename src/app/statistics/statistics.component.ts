import { Component, OnInit } from '@angular/core';
import Dexie from 'dexie'

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.sass']
})
export class StatisticsComponent implements OnInit {
	db: any
	count: number = 0
	birthdayboys: number = 0

  constructor()
  {
    this.db = new Dexie('database')
    this.db.version(1).stores({
      contacts: '++id, name, phone, address, birthdate'
    })
    this.db.open().catch(err => console.log(err))
    this.db.table('contacts').count().then(res => this.count = res)
    this.db.table('contacts').toArray().then(res => res.map(i => {
    	let today = new Date(),
    			ibdate = new Date(i.birthdate)
    	if (today.getDate() == ibdate.getDate() && today.getMonth() == ibdate.getMonth())
    		this.birthdayboys++

    }))
  }

  ngOnInit(): void {
  }

}
